module.exports = function (grunt) {
    grunt.initConfig({
        distFolder: 'web/assets/dist',
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            bundled: {
                src: '<%= distFolder %>/bundled.css',
                dest: '<%= distFolder %>/bundled.min.css'
            }
        },
        uglify: {
            js: {
                files: {
                    '<%= distFolder %>/bundled.min.js': ['<%= distFolder %>/bundled.js']
                }
            }
        },
        concat: {
            options: {
                stripBanners: true
            },
            css: {
                src: [
                    'node_modules/mdbootstrap/css/bootstrap.min.css',
                    'web/assets/css/mdb.min.css',
                    'web/assets/custom/css/*',
                ],
                dest: '<%= distFolder %>/bundled.css'
            },
            js: {
                src: [
                    'node_modules/mdbootstrap/js/jquery-3.1.1.min.js',
                    'node_modules/mdbootstrap/js/tether.min.js',
                    'node_modules/mdbootstrap/js/bootstrap.min.js',
                    'web/assets/js/mdb.min.js',
                    'web/assets/custom/js/*'
                ],
                dest: '<%= distFolder %>/bundled.js'
            }
        },
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'web/assets/scss',
                    src: ['*.scss'],
                    dest: 'web/assets/custom/css',
                    ext: '.css'
                }]
            }
        },
        copy: {
            images: {
                expand: true,
                cwd: 'web/assets/images',
                src: '*',
                dest: '<%= distFolder %>/images/'
            },
            fonts: {
                expand: true,
                cwd: 'node_modules/mdbootstrap/font',
                src: '*',
                dest: '<%= distFolder %>/font/'
            }
        },
        watch: {
            scripts: {
                files: [
                    'web/assets/custom/js/*.js',
                    'web/assets/scss/**/*.scss'
                ],
                tasks: ['default'],
                // tasks: ['dev'],
                options: {
                    spawn: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['copy', 'sass', 'concat', 'cssmin', 'uglify']);
    grunt.registerTask('dev', ['copy', 'sass', 'concat']);
};