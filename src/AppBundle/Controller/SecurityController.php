<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 11/5/16
 * Time: 10:05 PM
 */

namespace AppBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Igsem\AdminBundle\Entity\User;
use Igsem\AdminBundle\Form\UserRegistrationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class SecurityController
 *
 * @package AppBundle\Controller
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @Template()
     *
     * @return array
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \OutOfBoundsException
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return [
            'last_username' => $lastUsername ,
            'error'         => $error ,
        ];
    }

    /**
     * @Route("/logout", name="logout")
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function logoutAction()
    {
        return new Response();
    }


    /**
     * @Route("/register", name="register")
     * @Template()
     * @param Request $request
     *
     * @return array
     *
     * @throws \LogicException
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserRegistrationType::class , $user);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $this->getDoctrine()->getManager()->persist($user);
            try {
                $this->getDoctrine()->getManager()->flush();
                $this->authenticateUser($user);

                return $this->redirectToRoute('homepage');
            } catch (UniqueConstraintViolationException $e) {
                $form->addError(new FormError('Pouzivatel s tymto usernamemom uz existuje'));
            }
        }

        return [
            'form' => $form->createView() ,
        ];
    }

    /**
     * Manual authentication
     *
     * @param User $user
     *
     * @throws \InvalidArgumentException
     */
    private function authenticateUser(User $user)
    {
        $providerKey = 'main'; // your firewall name
        $token = new UsernamePasswordToken($user , null , $providerKey , $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
    }
}