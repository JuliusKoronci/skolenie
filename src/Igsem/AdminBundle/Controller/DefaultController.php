<?php

namespace Igsem\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IgsemAdminBundle:Default:index.html.twig');
    }
}
