<?php

namespace Igsem\AdminBundle\Controller;

use Igsem\AdminBundle\Entity\User;
use Igsem\AdminBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('IgsemAdminBundle:User')->findAll();

        return $this->render('@IgsemAdmin/User/index.html.twig' , [
            'users' => $users ,
        ]);
    }

    /**
     * Creates a new user entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('Igsem\AdminBundle\Form\UserType' , $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $user->getPassword();
            $encodedPassword = $this->get('security.password_encoder')->encodePassword($this->getUser() , $password);
            $user->setPassword($encodedPassword);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_show' , ['id' => $user->getId()]);
        }

        return $this->render('@IgsemAdmin/User/new.html.twig' , [
            'user' => $user ,
            'form' => $form->createView() ,
        ]);
    }

    /**
     * Finds and displays a user entity.
     *
     * @param User $user
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('@IgsemAdmin/User/show.html.twig' , [
            'user'        => $user ,
            'delete_form' => $deleteForm->createView() ,
        ]);
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request , User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm(UserType::class , $user , ['edit' => true]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_edit' , ['id' => $user->getId()]);
        }

        return $this->render('@IgsemAdmin/User/edit.html.twig' , [
            'user'        => $user ,
            'edit_form'   => $editForm->createView() ,
            'delete_form' => $deleteForm->createView() ,
        ]);
    }

    /**
     * Deletes a user entity.
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     */
    public function deleteAction(Request $request , User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('user_delete' , ['id' => $user->getId()]))
                    ->setMethod('DELETE')
                    ->getForm();
    }
}
