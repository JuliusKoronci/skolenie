<?php

namespace Igesm\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Igsem\AdminBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserFixture
 *
 * @package Igesm\AdminBundle\DataFixtures\ORM
 */
class UserFixture implements FixtureInterface , ContainerAwareInterface , OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 10; $i++) {
            $user = new User();
            $user
                ->setEmail($i . 'test@test.sk')
                ->setName($i . 'name')
                ->setSurname($i . 'surname');

            $plainPassword = 'testheslo';
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user , $plainPassword);
            $user->setPassword($encoded);

            if (0 === $i % 2) {
                $user->setNick('NickName');
            }

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}