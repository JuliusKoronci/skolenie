<?php

namespace Igsem\AdminBundle\Form;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue;

/**
 * Class UserType
 *
 * @package Igsem\AdminBundle\Form
 */
class UserRegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $builder
            ->add('email')
            ->add('password' , RepeatedType::class , [
                'type'            => PasswordType::class ,
                'invalid_message' => 'The password fields must match.' ,
                'options'         => ['attr' => ['class' => 'password-field']] ,
                'required'        => true ,
                'first_options'   => [
                    'label' => 'Password' ,
                    'attr'  => [
                        'class' => 'form-control' ,
                    ] ,
                ] ,
                'second_options'  => [
                    'label' => 'Repeat Password' ,
                    'attr'  => [
                        'class' => 'form-control' ,
                    ] ,
                ] ,

            ])
            ->add('nick')
            ->add('surname')
            ->add('name')->add('recaptcha' , EWZRecaptchaType::class , [
                'mapped'      => false ,
                'constraints' => [new IsTrue()] ,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Igsem\AdminBundle\Entity\User' ,
            'tags'       => [] ,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'igsem_adminbundle_user';
    }


}
