<?php

namespace Igsem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType
 *
 * @package Igsem\AdminBundle\Form
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $builder
            ->add('email')
            ->add('name')
            ->add('surname')
            ->add('nick')
            ->add('isActive');
        if (false === $options['edit']) {
            $builder->add('password');
        }
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Igsem\AdminBundle\Entity\User' ,
            'edit'       => false ,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'igsem_adminbundle_user';
    }


}
